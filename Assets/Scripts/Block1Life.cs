﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block1Life : Block
{
    // Start is called before the first frame update
    protected override void Start()
    {
        life=1;
        Colorear();
    }
    protected void Colorear(){
         SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float red;
        float green;
        float blue;


        red = 1f;
        green =  0f; 
        blue = 0f;

        sp.color = new Color(red, green, blue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Velocidad;
    public float LimiteX;
    private float Position;
    private float direction;
    
    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Horizontal");

        Position = transform.position.x + direction*Velocidad*Time.deltaTime;
        
        //Limite Player
        if(Position > LimiteX){
            Position = LimiteX;
        }
        else if(Position < -LimiteX){
            Position = - LimiteX;
        }
        transform.position = new Vector3(Position, transform.position.y, transform.position.z);

        private void OnTriggerEnter2D(Collider2D other){
            if(other.tag == "killer"){
                Destroy(gameObject);
            }
        }
    }
}

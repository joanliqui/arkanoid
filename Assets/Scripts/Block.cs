﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public float life = 2;
    protected virtual void Start(){
        Colorear();
    }

    protected void Colorear(){
         SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float red;
        float green;
        float blue;


        red = Random.Range(0f, 1f);
        green = Random.Range(0f, 1f);
        blue = Random.Range(0f, 1f);

        sp.color = new Color(red, green, blue);
    }

    protected void Negro(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float red;
        float green;
        float blue;


        red = 0;
        green = 0;
        blue = 0;

        sp.color = new Color(red, green, blue);

    }

    public virtual void TouchBall(){
        life --;
        if(life==0){
        Destroy(gameObject);
        }
        else{
            Negro();
        }
    }
}

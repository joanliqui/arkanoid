using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_killer : Block
{
    public GameObject killer;
    

    public override void TouchBall(){
        GameObject go = Instantiate (killer);
        go.transform.position = transform.position;
        Destroy(gameObject);
        }
    }

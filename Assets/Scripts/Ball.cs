﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 Velocity;

    void Awake(){
        ballPosition = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + Velocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + Velocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);
    }

    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag == "MuroV"){
            Velocity.y *= -1;
        }
        else if (other.gameObject.tag == "MuroH"){
            Velocity.x *= -1;
        }
        else if(other.gameObject.tag == "Player"){
            Velocity.y *= -1;
        }
        else if(other.gameObject.tag =="Block"){
            Velocity.y *= -1;
            other.gameObject.GetComponent<Block>().TouchBall();
        }
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block2Life : Block
{
    // Start is called before the first frame update
    protected override void Start()
    {
        life=2;
        Colorear();
    }
    protected void Colorear(){
         SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float red;
        float green;
        float blue;


        red = 0f;
        green = 1f; 
        blue = 0f;

        sp.color = new Color(red, green, blue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

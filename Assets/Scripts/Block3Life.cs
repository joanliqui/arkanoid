﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block3Life : Block
{
    // Start is called before the first frame update
    protected override void Start()
    {
        life=3;
        Colorear();
    }
    protected void Colorear(){
         SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float red;
        float green;
        float blue;


        red = 0f;
        green = 0f; 
        blue = 1f;

        sp.color = new Color(red, green, blue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
